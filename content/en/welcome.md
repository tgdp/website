---
title: Welcome to The Good Docs Project
linkTitle: Welcome
lastmod: 2022-12-03T02:00:33.466Z
---

{{% blocks/lead color="primary" %}}

# Welcome to The Good Docs Project

We're glad you are interested in joining us.

{{% /blocks/lead %}}

{{% blocks/section color="white" type="section" %}}

To get you started in our community, we'd like to invite you to our next Welcome Wagon meeting.
At this 1-hour orientation meeting, you'll get:

* A brief overview of our project's goals and mission.
* Some information about our community and resources that are available for your support.
* An overview of our key initiatives and [working groups](../working-group/).
* An introduction to the contributing process for at least one [working group](../working-group/) of your choice.
* A chance to meet other project members and leaders.

In order to register for this meeting, we need you to fill out the following form.
Your privacy will be respected, and this information will only be shared with people in the project on a need-to-know basis.

**After submitting this form, please allow 1-5 business days for someone from our project to respond to your request. Response times may vary.**

<script src="https://www.cognitoforms.com/f/seamless.js" data-key="6tiz94CtckepmzIMR8C54Q" data-form="2"></script>

{{% /blocks/section %}}
