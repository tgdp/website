---
title: UX and outreach
leads:
  - "Carrie Crowe Wattula"
  - "Alyssa Rock"
meeting: Thursdays (US)/Fridays (APAC) - Check time on [Community calendar](https://thegooddocsproject.dev/community/#calendar)
status: Active
description: Builds our user community through outreach and by conducting user research and designing a better experience for using our templates.
slackName: ux-and-outreach
slackID: C023G344YAJ
notesURL: https://docs.google.com/document/d/1x6SExdCncsbRb3XMUYyshXcPo5OIFxT81fvComNJjkc/edit?usp=sharing
draft: false
---

The UX and outreach working group is critical to the Good Docs Project mission because they want to get templates and other resources produced by our great community into the hands of users. For that reason, their main objective is to expand and grow our user community and improve our overall user experience.

To achieve that goal, the UX and outreach group conducts user research to better understand our users' needs. They will take that research and design an ideal experience for consuming our templates and other resources. They are also responsible for helping get the word out about our project through our website, blogs, and social media.

Join this group if you are interested in UX, user research, blogging, social media, and high-level discussions about content strategy!
