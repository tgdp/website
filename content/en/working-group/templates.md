---
title: Templates
leads:
  - "Alyssa Rock"
  - "Deanna Thompson"
  - "Gayathri Krishnaswamy"
  - "Joanne Fung"
  - "Michael Park"
meeting: Check the [Community calendar](https://thegooddocsproject.dev/community/#calendar)
status: Active
description: Creates high-quality templates for common content types used in a variety of documentation projects.
slackName: templates
slackID: CPXECK804
notesURL: https://docs.google.com/document/d/1iRxB0T7FoVjIeG_LawbCzcLfXCILlpFdzn-4BPDGR9Y/edit?usp=sharing
draft: false
---

This group is great for both new and experienced technical writers.
We can always use more template writers.

If you’re not sure which working group to join, we generally recommend joining a template writing group.
The template project is our flagship project and the processes for contributing a template are well-established.

We have three template working groups you can join depending on which time zone works best for you:

- **Team Alpaca** - For the APAC and US regions
- **Team Macaw** - For the APAC and EMEA regions
- **Team Dolphin** - For the EMEA and US regions