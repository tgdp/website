---
title: Chronologue
leads:
  - "Valeria Hernandez"
meeting: Saturdays - Check time on [Community calendar](https://thegooddocsproject.dev/community/#calendar)
status: Active
description: Acts as quality assurance by testing the Good Docs templates and creating high-quality examples of our templates for the Chronologue mock tool.
slackName: chronologue-docs
slackID: C016L3962CU
notesURL: https://docs.google.com/document/d/1Pr8ncPr2qAnwE4umr4lFVRiUhBGM1n8wESKgrz2YEoo/edit?usp=sharing
draft: false
lastmod: 2022-07-13T16:52:41.181Z
---

The Chronologue is mock tool that needs your help to document it! It's a telescope that uses wormhole technology to view astronomical events at any point in the past, present, or future.

The goal of the Chronologue is to document a mock tool to create an example of what good documentation looks like.

We have a strong need for team members to help us test our templates, create examples of effective documentation, and improve the overall quality of our templates.
It doesn’t matter if you have a writing background or not since we intend our templates to be used by people from a variety of backgrounds.

If you have development skills, we can also use your help developing our fake tool!

Be aware that this working group is difficult to join if you are in the APAC region because this working group currently doesn’t meet at APAC friendly times yet.
If you are in the APAC region, let us know and we’ll consider setting up new times based on how many people are interested.