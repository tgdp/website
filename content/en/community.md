---
title: Community and Contact
menu:
  main:
    name: Community
    weight: 20
aliases:
  - /contact.html
  - /calendar
lastmod: 2022-04-25T12:28:34.186Z
---

{{% blocks/lead color="primary" %}}
# The Good Docs Project Community

We are a community of technical writers, developers, UX designers, and more who work together to make software documentation better.
Whether you are new to documentation or a seasoned technical writer, joining our community will give you opportunities to mentor and be mentored by others, build your professional network, improve your documentation skills, and help make software documentation better.

{{% /blocks/lead %}}

{{< blocks/community_links color="200" >}}

{{% blocks/section color="white" type="section" %}}

## How to join and contribute

The best way to join our project is to register for a [Welcome Wagon meeting](/welcome/).
At this 30-minute orientation meeting, you'll get:

* A brief overview of our project's goals and mission.
* A bit of information about our community and reasons to consider joining.
* An overview of our key initiatives and working groups that you might consider contributing to.
* A chance to tell us your goals for contributing as well as your current skill/experience levels to see if we can find a working group and a good first task for you.

Sign up by filling out the [registration form](/welcome/) today!

## Communication forums

Everyone is welcome to join our forums. When engaging with members of our project, we ask that you follow our [Code of Conduct](/code-of-conduct/).

### Calendar

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.7/jstz.js"></script>

<iframe id="calendar_container" src="https://calendar.google.com/calendar/embed?src=gooddocsproject%40gmail.com&ctz=Australia%2FSydney" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

<script type="text/javascript">
  var timezone = jstz.determine();
  var pref = 'https://www.google.com/calendar/embed?src=gooddocsproject%40gmail.com&ctz=';
  var iframe_src = pref + encodeURIComponent(timezone.name().replace(' ' ,''));
  document.getElementById('calendar_container').src = iframe_src;
</script>

Our calendar lists our video meetings. Connection details are in the meeting details.

If you'd like to contribute regularly, you should ask the meeting lead to add you to the meeting invite. This means you can:
* Log in without approval from the meeting lead.
* Have the invite added to your personal calendar.

You can view Good Docs Project meetings and events on your personal calendar by subscribing to the Good Docs Project public calendar.
To subscribe:

1. Click **+** next to the **Google Calendar** link in the lower right corner of the Good Docs public calendar.
2. When your personal Google calendar loads, you'll see the **Add calendar** dialog box. Click **Add**.

### Slack

We regularly chat in Slack. You can receive an invite to our Slack workspace by registering for a [Welcome Wagon meeting](/welcome/).


## Additional resources and information

* [Who we are](/who-we-are)
* [Roles and responsibilities](/roles)
* [Working groups](/working-group)
* Decisions
  * [Voting process](/decisions)
  * [Prioritization guidelines and principles](/prioritization)
  * [Proposal selection process](/proposal-selection)
  * [Decision log](/decisions/#project-decision-log-archives)

{{% /blocks/section %}}
