---
title: Code Of Conduct
lastmod: 2022-09-26T16:58:47.189Z
---

{{% blocks/lead color="primary" %}}

# Code of Conduct

Here we define the Code of Conduct for The Good Docs Project.

**Version:** 1.1 - **Last updated:** August 2023

{{% /blocks/lead %}}


{{% blocks/section color="white" type="section" %}}

The Good Docs Project and its members, contributors, sponsors, and leaders
pledge to make participation in our community a positive, inclusive, and safe
experience for all. The Good Docs Project encourages contributions from everyone
who shares our goals and wants to participate in a healthy, constructive, and
professional manner.

This code of conduct aims to support a community where all people should feel
safe to participate, to introduce new ideas, and to inspire others. This
includes everyone, regardless of: ability, age, background, body size or type,
caste, disability (either visible or invisible), education, ethnicity, family
status, gender, gender identity and expression, geographic location, level of
experience, marital status, nationality or national origin, native language,
personal appearance, race, religion, sexual identity and orientation,
socio-economic status, or any other dimension of diversity.

Openness and respectful collaboration are core values at the Good Docs Project.
We are committed to being a community that everyone feels good about joining,
and we will always work to treat everyone well. No matter how you identify
yourself or how others perceive you, you are welcome. We gain strength from
diversity and actively seek participation from those who enhance it.

These guidelines exist to enable all Good Docs community members to collaborate
effectively. As such, this document outlines both expected behavior and behavior
that will not be tolerated.


## Expected behavior

We expect our members, contributors, and leaders to:

- Participate in the community actively and authentically. Your meaningful
  contributions add to the health and longevity of this community.
- When you make commitments, do your best to keep those commitments. Other
  community members will trust you and build confidence in you if you fulfill
  your promises. If something may prevent you from keeping a commitment or if
  you discover you won't be able to complete a task on time, try to notify
  others as soon as possible.
- Attempt collaboration before conflict. Seek out and be respectful of differing
  opinions, styles, viewpoints, and experiences.
- Give and accept constructive feedback, gracefully. When expressing
  disagreement, be professional and respectful. Be open to learning from and
  educating others and educating others where needed.
- Demonstrate empathy and kindness toward other people. Be considerate and
  respectful in your word choice, speech, and actions. Show respect with the
  terms you use to address others.
- Look out for those around you in the community, especially if you are in a
  position of influence. Alert the Good Docs Project [community managers](https://thegooddocsproject.dev/who-we-are/#community-managers)
  if you notice a dangerous situation, someone in distress, or violations of
  this code of conduct, even if they seem inconsequential.
- Ensure all your contributions to the project are your own original work and
  are expressed using your own words. If you borrow from or take inspiration
  from the ideas, words, or work of other people, acknowledge your sources.
- Gracefully accept responsibility and apologize to those affected by any
  mistakes, whether made intentionally or unintentionally. If someone says they
  have been harmed through your words or actions, listen carefully, make amends,
  learn from the experience, and correct the behavior going forward.
- Focus on what is best for the overall community, not just for us as
  individuals. Ensure that leadership roles and opportunities are
  well-distributed across the community membership and not just centered in one
  person or the same few people. To help our community develop and build up
  leaders at every level of the Good Docs Project, ensure that you share
  knowledge with others as much as possible and be mindful of fostering healthy
  dialogue where no one voice dominates a conversation.


## Behavior that will not be tolerated

The following behaviors are unacceptable within our community, whether they
occur online, offline, privately, or publicly:

- Violent threats or language directed against another person.
- Sexist, racist, or otherwise discriminatory jokes and language.
- Deliberate intimidation, stalking, or following (online or in person).
- Personal insults, especially those related to gender, sexual orientation,
  race, religion, or disability. This includes misgendering, name calling, and
  mockery.
- Trolling (posting controversial comments in order to provoke others),
  insulting, or making derogatory comments.
- The use of sexualized language, jokes, or imagery, and sexual attention,
  inappropriate physical contact, or sexual advances of any kind.
- Bullying, tone-policing (attacking a person’s tone rather than the content of
  their message), or repeatedly dominating a topic of conversation (such as
  regularly talking over another person or not inviting discussion from others
  where appropriate).
- Publishing or threatening to publish others’ private information without their
  explicit permission. Private information includes physical addresses, phone
  numbers, email addresses, and emails or other communications sent privately or
  non-publicly.
- Deliberate "outing" of any private aspect of a person's identity without their
  consent except as necessary to protect vulnerable people from intentional
  abuse. This includes sharing personally identifying information ("doxing").
- Excessive or unnecessary profanity.
- Deliberate misgendering.
- Knowingly making harmful false claims about a person.
- Pushing a person to drink alcohol when they don’t want to drink, or deceiving
  someone into drinking alcohol.
- Plagiarism, which is the use of someone else’s work, words, or ideas as your
  own.
- Marketing to our community members either individually or collectively without
  their express approval. Solicitations on behalf of your business should be
  restricted to designated channels or areas. For more information about
  appropriate ways to market your business to the community, ask one of the Good
  Docs Project [community managers](https://thegooddocsproject.dev/who-we-are/#community-managers).
- Repeatedly communicating with a community member who has asked you to leave
  them alone.
- Sustained disruption of community events, including meetings and
  presentations.
- Recording or photography at community meetings and events without explicit
  permission, including inviting an OtterPilot or an A.I. bot to take notes at
  an official project meeting without the permission of each meeting attendee.
- Advocating for, or encouraging, any of the above behavior.
- Other conduct which could reasonably be considered inappropriate in a
  professional setting.


## Consequences of unacceptable behavior

Unacceptable behavior from any Good Docs Project community member, contributor,
sponsors, or leaders will not be tolerated. We expect everyone to comply with
requests to stop unacceptable behavior immediately.

If a community member engages in unacceptable behavior, any community member
should report the incident to the Good Docs Project
[community managers](https://thegooddocsproject.dev/who-we-are/#community-managers).
The community managers will investigate the incident to determine the
incident’s severity and overall impact to the community. See our
[Code of Conduct response plan](https://gitlab.com/tgdp/governance/-/blob/main/CODE_OF_CONDUCT_RESPONSE_PLAN.md)
for more details.

Depending on the risk and impact level, the community managers may respond by
requiring:

- **Correction:** A private, written warning from community managers, providing
  clarity around the nature of the violation and an explanation of why the
  behavior was inappropriate and what behavior is expected going forward. A
  public apology may be requested.
- **Warning:** A warning with consequences for continued behavior. No
  interaction with the people involved, including unsolicited interaction with
  community managers, for a specified period of time. This includes avoiding
  interactions in community spaces as well as external channels like social
  media. Violating these terms may lead to a temporary or permanent ban.
- **Temporary ban:** A temporary ban from any sort of interaction or public
  communication with the community for a specified period of time. No public or
  private interaction with the people involved, including unsolicited
  interaction with community managers, is allowed during this period.
  Violating these terms may lead to a permanent ban. Readmittance to the
  community usually requires an additional meeting with a community manager to
  ensure future compliance.
- **Permanent ban:** A permanent ban from any sort of public interaction within
  the community.

The action taken is at the discretion of the Good Docs Project
[community managers](https://thegooddocsproject.dev/who-we-are/#community-managers).
Participants are expected to comply immediately, and further action may be taken
in case a participant does not comply.

Every community member is entitled to one appeal using the
[same process for reporting a Code of Conduct incident](#reporting-an-incident).
Community members are expected to comply with the requested action while appeals
are being considered. After an appeal has been resolved, the decision is
considered final.


## Reporting an incident

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported to one of the Good Docs Project [community managers](https://thegooddocsproject.dev/who-we-are/#community-managers). If you would like to discuss your concerns or if you have
personally experienced unacceptable behavior, please reach out to a community
manager as soon as possible.

We encourage reaching out to a community manager, even if you are unsure
whether something counts as a code of conduct incident or even if the situation
is merely something you observed and did not happen to you directly.

Please reach out as soon as possible if:

- You would like to discuss any concerns.
- You have personally experienced unacceptable or potentially unacceptable
  behavior.
- You want to report a situation happening to someone else.


## Addressing code of conduct reports

All complaints will be reviewed and investigated promptly and fairly.
If possible, community managers will recuse themselves in cases where there is a
conflict of interest. See our
[Code of Conduct response plan](https://gitlab.com/tgdp/governance/-/blob/main/CODE_OF_CONDUCT_RESPONSE_PLAN.md)
for more details.

An internal record will be kept of all incidents. However, all community leaders
are obligated to respect the privacy and security of the reporter of any
incident. In some cases, the community managers may determine that a public
statement will need to be made. If that's the case, the identities of all
victims and reporters will remain confidential unless those individuals instruct
us otherwise.

If you feel you have been unfairly accused of violating these guidelines, please
follow the [same process for reporting a Code of Conduct incident](#reporting-an-incident).


## Scope

These guidelines apply to all members of the Good Docs Project and to all Good
Docs Project activities, including but not limited to:

- Representing the Good Docs Project at public events and in social media.
- Participating in the Good Docs Project meetings and events, whether virtual or
  in person.
- Participating in the Good Docs Project’s related messaging forums, including
  our Slack workspace and mailing list and other Good Docs Project-related
  correspondence.
- Interpersonal communications between Good Docs Project community members,
  whether virtual or in person.

While this code of conduct applies specifically to the Good Docs Project’s work
and community, it is possible for actions taken outside of the Good Docs
Project’s online or in-person spaces to have a deep impact on community health
if it concerns the Good Docs Project or its members in some way. The community
managers reserve the right to consider communications or actions that occur
outside of official Good Docs Project spaces if they have a demonstrated impact
on one or more community members.


## Resources

For more information, see:

- The current team of Good Docs Project [community managers](https://thegooddocsproject.dev/who-we-are/#community-managers)
- The Good Docs Project [Code of Conduct response plan](https://gitlab.com/tgdp/governance/-/blob/main/CODE_OF_CONDUCT_RESPONSE_PLAN.md)


In creating this code of conduct, the authors adapted or were inspired by the
following resources, listed alphabetically:

- [Ada Initiative](https://adainitiative.org/2014/02/18/howto-design-a-code-of-conduct-for-your-community/)
- [Apache Foundation Code of Conduct](http://www.apache.org/foundation/policies/conduct#code-of-conduct)
- [Citizen Code of Conduct](https://github.com/stumpsyn/policies/blob/master/citizen_code_of_conduct.md)
- [Contributor Covenant 2.0](https://www.contributor-covenant.org/version/2/0/code_of_conduct/)
- [Django Code of Conduct](https://www.djangoproject.com/conduct/)
- [Mozilla Community Participation Guidelines](https://www.mozilla.org/en-US/about/governance/policies/participation/)
- [No more rock stars: how to stop abuse in tech communities](https://hypatia.ca/2016/06/21/no-more-rock-stars/)
- [Otter Tech Code of Conduct Enforcement Training](https://otter.technology/code-of-conduct-training/)
- [Rust Community Code of Conduct](https://www.rust-lang.org/policies/code-of-conduct)

{{% /blocks/section %}}
